#!/usr/bin/python
# -*- coding: utf8 -*-
import requests, urllib
from bs4 import BeautifulSoup
import codecs

import threading

# Парсер сайта ulroad
class UlroadParser:
    # селектор элементов маршрутов
    nums_path = 'body > .menu_body > .bottom_body > .scroll-pane > ul.scroll_menu li > input.num_route'
    # селектор элементов остановок
    stop_ids_path = 'body > .menu_stops > .scroll-stops > ul.scroll_menu li > input.id_stop'
    # номера маршруток
    nums = ()
    # айдишники остановок
    stop_ids = ()
    # проксики
    proxies = ()
    # json остановок
    stops_json = None

    # базовый url
    api_url = 'http://www.ulroad.ru'
    # url для обработки
    stops_api_url = api_url + '/getstop_ajax.php'
    # url данных о маршруте
    route_api_url = api_url + '/kml/visual/'
    # url данных об остановках маршруток
    route_stops_api_url = api_url + '/get_stops_xml.php?o=0,'

    # маршруты
    routes = {}
    # остановки
    stops = {}
    # места остановок маршруток
    routes_stops = {}

    # загружает html из файла
    def load(self, raw_html):
        self.html = raw_html
        self.parse()

    # парсит html
    def parse(self):
        soup = BeautifulSoup(self.html, "html5lib")
        
        # парсим номера маршруток
        nums = soup.select(self.nums_path)
        self.nums = map(lambda el: el['value'], nums)
        
        # парсим айдишники остановок
        stop_ids = soup.select(self.stop_ids_path)
        self.stop_ids = map(lambda el: int(el['value']), stop_ids)

    # выполняет запрос по url
    # возвращает <Response>
    def download(self, url):
        r = requests.get(url, proxies = self.proxies)
        r.encoding = "utf8"

        if(r.status_code == requests.codes.ok):
            return r
        else:
            raise Exception("Error {}".format(r.status_code), url)

    # генератор json остановок. возвращает пару Ид=>Json 
    def get_stops(self):
        for stop_id in self.stop_ids:
            response = self.load_stop_by_id(stop_id)
            yield {stop_id : response.json()}

    # генератор xml маршрутов. возвращает пару Имя=>Текст
    def get_routes(self):
        for num in self.nums:
            response = self.load_route_by_num(num)
            yield {num : response.text}

    # генератор xml остановок маршруток. возвращает пару Имя=>Текст
    def get_routes_stops(self):
        for num in self.nums:
            response = self.load_route_stops_by_num(num)
            yield {num : response.text}

    # загружает остановку по ид
    def load_stop_by_id(self, stop_id):
        params = urllib.urlencode({'id' : stop_id})
        url = self.stops_api_url + '?' + params
        return self.download(url)

    # загружает маршрут по номеру
    def load_route_by_num(self, num):
        url = self.route_api_url + str(num) + '.xml'
        return self.download(url)

    # загружает места остановок маршрутки
    def load_route_stops_by_num(self, num):
        url = self.route_stops_api_url + str(num)
        return self.download(url)

    # сохранение в файл
    def save_to_file(self, name, value):
        with codecs.open(name, 'w', 'utf8') as f:
            f.write(value)
            f.close()

    # загрузка из файла
    def load_from_file(self, name):
        with codecs.open(name, 'r', 'utf8') as f:
            return f.read()
