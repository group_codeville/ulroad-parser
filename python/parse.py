#!/usr/bin/python
# -*- coding: utf8 -*-

from time import time
from UlroadParser import UlroadParser

import os.path, json

start_time = time()

parser = UlroadParser()
print(u"Загружаем главную страницу...")

if os.path.isfile('cached.html'):
    html = parser.load_from_file('cached.html')
else:
    rsp = parser.download(parser.api_url)
    html = rsp.text
    parser.save_to_file('cached.html', html)

parser.load(html)

out_dirs = ['stops', 'routes', 'routes_stops', 'nums']

for out_dir in out_dirs:
    try:
        os.makedirs(out_dir)
    except OSError:
        if os.path.exists(out_dir):
            pass
        else:
            raise

print(u"Начинаем парсинг...")
parser.parse()

print(u"Получаем json данные об остановках...")
for stop in parser.get_stops():
    parser.stops.update(stop)
    parser.save_to_file('stops/' + str(stop.keys()[0]) +'.json', json.dumps(stop.values()[0]))

print(u"Загружаем xml данные о маршрутах...")
for route in parser.get_routes():
    parser.routes.update(route)
    parser.save_to_file('routes/' + str(route.keys()[0]) + '.xml', route.values()[0])

print(u"Загружаем xml данные об остановках маршруток...")
for route_stop in parser.get_routes_stops():
    parser.routes_stops.update(route_stop)
    parser.save_to_file('routes_stops/' + str(route_stop.keys()[0]) + '.xml', route_stop.values()[0])

print(u"Готово!")

print(u"Время выполнения " + str(time() - start_time) + u" сек.")